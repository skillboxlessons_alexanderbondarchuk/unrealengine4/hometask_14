#include<iostream>
#include<iomanip>

void main() {
	/* lesson samples
	float a{ 5e1 }, b{ 5e-1 }, c{ 5e-10 };

	std::cout << "a = " << a << std::endl;
	std::cout << "b = " << b << std::endl;
	std::cout << std::fixed << std::setprecision(10) << "c = " << c << std::endl;
	*/
	std::string hello{ "Good evening!" };
	std::cout << hello << std::endl;
	std::cout << hello.length() << std::endl;
	std::cout << hello[0] << std::endl;
	std::cout << hello[hello.length() - 1] << std::endl;
}